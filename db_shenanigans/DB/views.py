from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.http import HttpRequest, JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Customer, ConsignmentNote, Firm, Ware, Log
import json
import datetime


@login_required(login_url='/logging/login')
def index(request):

    context = {
        'customers': Customer.objects.all(),
        'notes': ConsignmentNote.objects.all(),
        'firms': Firm.objects.all(),
        'wares': Ware.objects.all(),
        'logs': Log.objects.all(),
        'max': range(max([Customer.objects.count(), Customer.objects.count(),
                     Customer.objects.count(), Customer.objects.count(), Customer.objects.count()])),
        'user': request.user
    }

    if request.method == 'GET':
        if request.GET.get('default'):
            context['default'] = request.GET.get('default')
        else:
            context['default'] = 'ware'
        return render(request, 'DB/index.html', context)

    if request.method == 'DELETE':
        data = request.body.decode()
        data = json.loads(data)

        entity = globals()[data['table'].capitalize()]
        obj = entity.objects.get(id=data['id'])

        obj.delete()
        return HttpResponse()

    if request.method == 'PUT':
        data = request.body.decode()
        data = json.loads(data)

        entity = globals()[data['table'].capitalize()]
        obj = entity.objects.get(id=data['id'])

        for key in data:
            if key not in ['table', 'id']:
                print(data[key])
                if data[key] != getattr(obj, key):
                    setattr(obj, key, data[key])

        obj.save()
        return HttpResponse()


@login_required(login_url='/logging/login')
def add(request: HttpRequest):
    context = {
        'customer_list': Customer.objects.all(),
        'ware_list': Ware.objects.all(),
        'user': request.user
    }
    if request.method == 'GET':
        return render(request, 'DB/add.html', context)

    if request.method == 'POST':
        context['chosen_wares'] = []
        context['unfilled_wares'] = []
        for key in request.POST:
            if key.startswith('ware'):
                q_key = 'quantity' + str(key[4:])
                context['chosen_wares'].append((request.POST[key], request.POST[q_key]))
            elif key.startswith('quantity'):
                continue
            else:
                context[key] = request.POST[key]

        if request.POST['flag'] == 'e':
            if not request.POST['existing']:
                context['existing_error'] = True
                return render(request, 'DB/add.html', context)
            customer = request.POST['existing'].split(', ')

            if customer[1].isdecimal():
                db_customer = Customer.objects.get(full_name=customer[0], phone_number=int(customer[1]))
            else:
                db_customer = Customer.objects.get(full_name=customer[0], email=customer[1])
        else:
            if not request.POST['email'] and not request.POST['phone'] or not request.POST['name']:
                if not request.POST['name']:
                    context['name_error'] = True
                if not request.POST['email'] and not request.POST['phone']:
                    context['email_phone_error'] = True
                return render(request, 'DB/add.html', context)

            query = Customer.objects.filter(full_name=request.POST['name'])

            if request.POST['email']:
                query.filter(email=request.POST['email'])

            if request.POST['phone']:
                query.filter(phone_number=request.POST['phone'])

            if len(query) > 0:
                context['already_exists'] = True
                return render(request, 'DB/add.html', context)

            db_customer = Customer(full_name=request.POST['name'])
            db_customer.phone_number = request.POST['phone'] if request.POST['phone'].isdecimal() else None
            db_customer.email = request.POST['email'] if request.POST['email'] else None
            db_customer.save()

        c = ConsignmentNote(customer_id=db_customer)

        error = False
        for i in range(len(context['chosen_wares'])):
            if not context['chosen_wares'][i][0] or not context['chosen_wares'][i][1]:
                context['unfilled_wares'].append(i)
                error = True

        if error:
            return render(request, 'DB/add.html', context)

        c.save()

        for w_q in context['chosen_wares']:
            log = Log(consignment_note_id=c, ware_id=Ware.objects.get(name=w_q[0]),
                      ware_quantity=w_q[1], date=datetime.datetime.now())
            log.save()

        return redirect('/config')


@login_required(login_url='/logging/login')
def add_firm(request):
    context = {'user': request.user}
    if request.method == 'GET':
        return render(request, 'DB/add_firm.html')

    if request.method == 'POST':
        if Firm.objects.filter(name=request.POST['name']):
            context['error'] = True
            return render(request, 'DB/add_firm.html', context)

        f = Firm(name=request.POST['name'], phone_number=request.POST['number'], address=request.POST['address'])

        f.save()
        return redirect('/config')


@login_required(login_url='/logging/login')
def add_ware(request):
    context = {'firms': Firm.objects.all(), 'user': request.user}
    if request.method == 'GET':
        return render(request, 'DB/add_ware.html', context)

    if request.method == 'POST':
        if Ware.objects.filter(name=request.POST['name']):
            context['error'] = True
            return render(request, 'DB/add_ware.html', context)

        w = Ware(name=request.POST['name'], price=request.POST['price'],
                 firm_id=Firm.objects.get(name=request.POST['manufacturer']))

        w.save()
        return redirect('/config')


def info(request):
    if request.method == 'GET':
        return render(request, 'DB/info.html')
    else:
        data = request.body.decode()
        data = json.loads(data)

        if data['type'] == 'Phone':
            if data['query'].isdecimal() and Customer.objects.filter(phone_number=data['query']):
                c = Customer.objects.get(phone_number=data['query'])
            else:
                return HttpResponse(json.dumps({'number_err': 'true'}))
        else:
            if Customer.objects.filter(email=data['query']):
                c = Customer.objects.get(email=data['query'])
            else:
                return HttpResponse(json.dumps({'email_err': 'true'}))

        notes = ConsignmentNote.objects.filter(customer_id=c.id)
        logs = []
        logs_to_json = []

        for note in notes:
            logs.extend(Log.objects.filter(consignment_note_id=note.id))

        for log in logs:
            logs_to_json.append({
                'date': log.date.date().isoformat(),
                'quantity': log.ware_quantity,
                'ware': log.ware_id.name,
                'price': log.ware_id.price * log.ware_quantity
            })

        data = json.dumps(logs_to_json)

        return HttpResponse(data)

