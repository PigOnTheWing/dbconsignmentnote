from django import template

register = template.Library()


@register.filter(name='get')
def get(target_list, index):
    for item in target_list:
        if item.id == index:
            return item
