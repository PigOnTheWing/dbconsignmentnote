from django.db import models


class Firm(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.IntegerField()

    def __str__(self):
        return str(self.name)


class Ware(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=100)
    price = models.FloatField()
    firm_id = models.ForeignKey(Firm, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.name)


class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    full_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100, null=True)
    phone_number = models.IntegerField(null=True)

    def __str__(self):
        return str(self.full_name)


class ConsignmentNote(models.Model):
    id = models.AutoField(primary_key=True)
    customer_id = models.ForeignKey(Customer, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id) + ', ' + str(self.customer_id)


class Log(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField()
    ware_quantity = models.IntegerField()
    ware_id = models.ForeignKey(Ware, on_delete=models.CASCADE)
    consignment_note_id = models.ForeignKey(ConsignmentNote, on_delete=models.CASCADE)

