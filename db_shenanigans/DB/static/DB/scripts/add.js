$(document).ready(function () {
        $('.customer').chosen();
        $('.ware').chosen();
        $('.priceBox').forceNumeric();
        $('#phone_num').forceNumeric();
        $('.new').hide();
        $('.flag').hide();
        $('.flag').val('e');
    });

    $('#plus').click(function () {
        $('.ware').last().clone().appendTo('.placeholder');
        $('.ware').last().css('display', 'block');
        $('.ware').last().chosen();
        $('.chosen-container').last().css('display', 'inline-block');
        $('.priceBox').last().clone().appendTo('.placeholder');
        $('.priceBox').last().forceNumeric();
        $('.priceBox').last().css('margin-left', '5px');
        $('.placeholder').append('<br class="br">');
    });

    /*$('#add').submit(function (event) {
       if ($('.new').is(':visible') && $('#email').val() === '' && $('#phone_num').val() === '') {
           $('#error_message').show();
           event.preventDefault();
       }
    });*/

    $('#minus').click(function () {
        if ($('.ware').length > 1) {
            $('.ware').last().remove();
            $('.chosen-container').last().remove();
            $('.priceBox').last().remove();
            $('.br').last().remove();
        }
    });

    $('#new').click(function () {
        $('.existing').hide();
        $('.new').show();
        $('.flag').val('n');
    });

    $('#existing').click(function () {
        $('.new').hide();
        $('#name').val('');
        $('#email').val('');
        $('#phone_num').val('');
        $('.existing').show();
        $('.flag').val('e');
    });

    jQuery.fn.forceNumeric = function () {
         return this.each(function () {
             $(this).keydown(function (e) {
                 let key = e.which || e.keyCode;

                 if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                     key >= 48 && key <= 57 ||
                     key >= 96 && key <= 105 ||
                    key == 190 || key == 188 || key == 109 || key == 110 ||
                    key == 8 || key == 9 || key == 13 ||
                    key == 35 || key == 36 ||
                    key == 37 || key == 39 ||
                    key == 46 || key == 45)
                     return true;

                 return false;
             });
         });
    };