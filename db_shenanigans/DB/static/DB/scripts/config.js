 $(document).ready(function () {
        $('#default').trigger('click');
    });

    let switchTab = function (e, category) {
        if (!findActive('.confirmButton')) {
            let tabs = $('.tabs');

            for (let i = 0; i < tabs.length; i++) {
                $(tabs[i]).css('display', 'none');
            }

            let buttons = $('.tabButton');
            for (let i = 0; i < buttons.length; i++) {
                $(buttons[i]).removeClass('active');
            }

            $(category).css('display', 'block');
            $(this).attr('class', $(this).attr('class') + ' active');
        }
    };

    let findActive = function(selector) {
        let empty = $(document).find(selector).filter(function () {
            return $(this).is(':visible');
        });

        return empty.length;
    };

    $('tr').hover(function () {
        if ($(this).find('th').length === 0) {
            if ($(this).parent().parent().parent()[0].className === 'tabs') {
                let index = $(this).parent().children().index(this);
                let addButton = $('.changeButton').get(index - 1);
                let delButton = $('.delButton').get(index - 1);
                let confirmButton = $('.confirmButton').get(index - 1);
                if ($(confirmButton).is(':hidden')) {
                    if ($(addButton).is(':visible')) {
                        $(addButton).hide();
                        $(delButton).hide();
                    } else {
                        $(addButton).show();
                        $(delButton).show();
                    }
                }
            }
        }
    });

    $('.changeButton, .delButton').hover(function () {
        if ($(this).css('visibility') === 'visible') {
            $(this).parent().children().each(function () {
                if (this.className !== 'confirmButton' && this.className !== 'resetButton') {
                    $(this).show();
                }
            });
        }
    }, function () {
            $(this).parent().children().each(function () {
                if (this.className !== 'confirmButton' && this.className !== 'resetButton') {
                    $(this).hide();
                }
            });
    });

    let prepareData = function(index) {
        let table = $(".tabs[style='display: block;']");
        let data = {};
        data['table'] = table.attr('id');
        let cell = $(table.children().children().children().get(index)).children().get(0);
        data['attr'] = cell.textContent;
        return data;
    };

    $('.delButton').click(function () {
        let index = $(this).parent().parent().children().index($(this).parent()) + 1;
        let data = prepareData(index);
        $.ajax({
            url: $('#lnk').val(),
            method: 'DELETE',
            data: JSON.stringify(data),
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", $('[name=csrfmiddlewaretoken]').val());
            },
            success: function () {
                window.location.reload(true);
                /*let table = $(".tabs[style='display: block;']");
                $(table.children().children().children().get(index)).remove();*/
            }
        });
    });

    $('.changeButton').click(function () {
        $(this).hide();
        $($(this).parent().children()[1]).hide();
        $($(this).parent().children()[2]).show();
        $($(this).parent().children()[3]).show();
        let index = $(this).parent().parent().children().index($(this).parent()) + 1;
        let table = $(".tabs[style='display: block;']");
        $(table.children().children().children().get(index)).find('.swap').each(function () {
            $(this).hide();
        });
        $(table.children().children().children().get(index)).find('.changeable').each(function () {
            $(this).show();
        });
    });