from django.urls import path
from . import views

app_name = 'DB'
urlpatterns = [
    path('', views.info, name='info'),
    path('config', views.index, name='index'),
    path('add', views.add, name='add'),
    path('add_firm', views.add_firm, name='add_firm'),
    path('add_ware', views.add_ware, name='add_ware')
]
